import US from './assets/US.png';
import Logo from './assets/logo.png'
import Down from './assets/down.png';

const GetStarted = () => {

    return <div className="flex h-screen">
        <div className="bg-[#01264A] w-1/2 p-20 flex flex-col justify-between items-start">
            <div className="flex flex-col justify-between w-[95%] h-screen m-auto p-4">

                <img src={Logo} width="208" height="104" />


                <p className="text-5xl font-bold text-white">
                    Electronic
                    <br />  <span className="text-[#4AC97D]"> Diversity visa </span> <br />entry form</p>

                <p className="text-4xl text-white">
                    Here you will fill all <br />your information to <br />reach your dream.
                </p>

                <div className="border-[6px] border-r-0 border-y-0 border-l-[#white] pl-5">
                    <p className="text-justify text-white">This is a full copy of the official DV Lottery application form. Use it for training only. You will need to apply on the official website www.dvlottery.state.gov when it opens to take part in the lottery.</p>
                </div>
            </div>

        </div>

        <div className="bg-[white] w-1/2 flex flex-col justify-between p-20">
            <div className="flex flex-row justify-end gap-8">

                <div className="flex items-center gap-2 text-[#01264A]">
                    <img src={US} width="20" height="5" className="h-[20px]" />
                    English
                    <img src={Down} className='w-[20px] h-[10px]' />
                </div>

                <button className="px-10 py-2 text-base font-semibold text-[#01264A] border border-[#01264A] rounded">
                    Live Chat
                </button>



            </div>

            <div className="flex flex-col gap-10 m-auto">
                <p className="text-4xl text-[#01264A]">Are you ready to begin?</p>
                <div className="flex w-full">
                    <button className="text-white bg-[#4AC97D] p-3 w-full rounded-md text-lg font-semibold">Start</button>
                </div>
            </div>

            <div className="flex justify-end">
                <p className="text-[16px]">Copyright © 2016-2023 GovAssist, LLC All Rights Reserved</p>

            </div>

        </div>


    </div>

}

export default GetStarted;