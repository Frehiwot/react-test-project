import Logo from './assets/logo.png'
import US from './assets/US.png';
import Icon1 from './assets/🦆 icon _people_.png';
import Icon2 from './assets/🦆 icon _medal star_.png';
import Icon3 from './assets/🦆 icon _wallet check_.png';
import Icon4 from './assets/🦆 icon _clock_.png';
import Icon5 from './assets/🦆 icon _message_.png';
import Icon6 from './assets/🦆 icon _lovely_.png';
import Arrow from './assets/arrow.png'
import Document from './assets/🦆 icon _document text_.png';
import UP from './assets/up.png';
import Down from './assets/down.png';
import OutLineStar from './assets/🦆 icon _star_.png';
import TrustLogo from './assets/trustLogo.png';
import GroupStars from './assets/starGroups.png';
import Profile from './assets/Ellipse 1.png';
import Location from './assets/ic_outline-place.png';
import SideImage from './assets/undraw_personal_opinions_re_qw29 1.png';
import { Route, Routes } from 'react-router-dom';
import Home from './Home';
import GetStarted from './GetStarted';

const DATA = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/getStarted" element={<GetStarted />} />
    </Routes>
  );
}

export default App;
