import Logo from './assets/logo.png'
import US from './assets/US.png';
import Icon1 from './assets/🦆 icon _people_.png';
import Icon2 from './assets/🦆 icon _medal star_.png';
import Icon3 from './assets/🦆 icon _wallet check_.png';
import Icon4 from './assets/🦆 icon _clock_.png';
import Icon5 from './assets/🦆 icon _message_.png';
import Icon6 from './assets/🦆 icon _lovely_.png';
import Arrow from './assets/arrow.png'
import Document from './assets/🦆 icon _document text_.png';
import UP from './assets/up.png';
import Down from './assets/down.png';
import OutLineStar from './assets/🦆 icon _star_.png';
import TrustLogo from './assets/trustLogo.png';
import GroupStars from './assets/starGroups.png';
import Profile from './assets/Ellipse 1.png';
import Location from './assets/ic_outline-place.png';
import SideImage from './assets/undraw_personal_opinions_re_qw29 1.png';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';

const DATA = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function Home() {

    const navigate = useNavigate();

    const [show, setShow] = useState(true)

    return (
        <div className="w-full h-screen bg-green-600">

            {/* first section */}
            <div className=" bg-[url('./assets/Home.png')] bg-cover bg-no-repeat w-full p-4 gap-11 flex flex-col ">


                <div className="h-[119px] bg-[#1F3655] flex justify-between w-[95%] mx-auto py-3 px-5 rounded-3xl  items-center">
                    <img src={Logo} width="208" height="104" />
                    <div className="flex flex-row gap-8">

                        <div className="flex items-center gap-2 text-white">
                            <img src={US} width="20" height="5" className="h-[20px]" />
                            English
                            <img src={Down} className='w-[20px] h-[10px]' />
                        </div>

                        <button className="px-10 py-2 text-base font-semibold text-white border border-white rounded" onClick={() => navigate("getStarted")}>
                            Live Chat
                        </button>



                    </div>

                </div>

                <div className="flex flex-col md:flex-row  w-[95%] gap-4 mx-auto mt-3 pb-10">
                    <div className="flex flex-col w-full h-full gap-11">

                        {/* header */}
                        <h1 className="text-6xl font-bold">
                            <span className="text-[#4AC97D]">Win the right to live</span>
                            <br />
                            <span className="text-[white]">in the USA</span>
                        </h1>

                        {/* second section */}
                        <div className="flex flex-col md:flex-row border-[6px] border-r-0 border-l-[#D9D9D9] border-t-0 border-b-0 w-full gap-5 items-center p-3">
                            <p className="text-2xl font-semibold text-white">
                                The official deadline <br />is running, so hurry <br /> up and apply here!
                            </p>

                            <div className="flex items-center justify-center gap-2">

                                <div className="bg-[#080F1A] text-white rounded-[16px] px-6 py-2 h-fit">
                                    <p className="font-bold text-[36px]">27</p>
                                    <p className="font-[300px]">DAYS</p>
                                </div>

                                <div className="bg-[#080F1A] text-white rounded-[16px] px-6 py-2 h-fit">
                                    <p className="font-bold text-[36px]">24</p>
                                    <p className="font-[300px]">Hours</p>
                                </div>

                                <div className="bg-[#080F1A] text-white rounded-[16px] px-6 py-2 h-fit">
                                    <p className="font-bold text-[36px]">60</p>
                                    <p className="font-[300px]">Minutes</p>
                                </div>

                                <div className="bg-[#080F1A] text-white rounded-[16px] px-6 py-2 h-fit">
                                    <p className="font-bold text-[36px]">60</p>
                                    <p className="font-[300px]">Seconds</p>
                                </div>
                            </div>

                        </div>

                        {/* list */}

                        <div className="flex flex-col gap-4 text-white">
                            <div className="flex gap-2">
                                <img src={Icon1} className="w-[31px] h-[31px]" />
                                <p className="text-base font-[500px]">50,000 people and their families will Live, Work and Study in USA</p>
                            </div>
                            <div className="flex gap-2">
                                <img src={Icon2} className="w-[31px] h-[31px]" />
                                <p className="text-base font-[500px]">50,000 people and their families will Live, Work and Study in USA</p>
                            </div>
                            <div className="flex gap-2">
                                <img src={Icon3} className="w-[31px] h-[31px]" />
                                <p className="text-base font-[500px]">50,000 people and their families will Live, Work and Study in USA</p>
                            </div>
                            <div className="flex gap-2">
                                <img src={Icon4} className="w-[31px] h-[31px]" />
                                <p className="text-base font-[500px]">50,000 people and their families will Live, Work and Study in USA</p>
                            </div>
                            <div className="flex gap-2">
                                <img src={Icon5} className="w-[31px] h-[31px]" />
                                <p className="text-base font-[500px]">50,000 people and their families will Live, Work and Study in USA</p>
                            </div>
                            <div className="flex gap-2">
                                <img src={Icon6} className="w-[31px] h-[31px]" />
                                <p className="text-base font-[500px]">50,000 people and their families will Live, Work and Study in USA</p>
                            </div>
                        </div>
                    </div>
                    <div className="w-full h-full ">
                        <div className="flex flex-col gap-5 bg-white rounded-[45px] shadow-md p-14 w-[95%] m-auto">
                            <div className="flex gap-3  font-semibold text-[#4AC97D]">
                                <img src={Arrow} className="w-[33px] h-[24px]" />
                                <p>  Check now for free!</p>
                            </div>

                            <h1 className="text-5xl text-[#01264A] font-bold">Green card eligibility</h1>

                            <div className="flex flex-col gap-5">
                                <div className="grid grid-cols-2 gap-2">
                                    <input placeholder='First Name' className="rounded-md bg-[#F3F3F3] text-lg p-3" />
                                    <input placeholder='First Name' className='rounded-md bg-[#F3F3F3] text-lg p-3' />
                                </div>
                                <div className="grid grid-cols-2 gap-2">
                                    <input placeholder='First Name' className="rounded-md bg-[#F3F3F3] text-lg p-3" />
                                    <input placeholder='First Name' className='rounded-md bg-[#F3F3F3] text-lg p-3' />
                                </div>


                                <div classsName="flex w-full space-x-2">
                                    <select className="rounded-md bg-[#F3F3F3]  text-lg p-3 w-full" >
                                        <option value="" disabled className="text-[#D2D2D2]" selected>Your Country of bitrh</option>
                                    </select>
                                </div>

                                <div classsName="flex w-full space-x-2">
                                    <select className="rounded-md bg-[#F3F3F3]  text-lg p-3 w-full" >
                                        <option value="" disabled className="text-[#D2D2D2]" selected>Your Martial Status</option>
                                    </select>
                                </div>
                            </div>

                            {/* checkbox */}
                            <div className="flex gap-2">
                                <input type="checkbox" />
                                <p>Yes, I finished high school OR I have qualifying training.</p>
                            </div>

                            <div className="flex w-full">
                                <button className="text-white bg-[#4AC97D] p-3 w-full rounded-md text-lg font-semibold">Continue</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            {/* second section */}
            <div className="flex flex-col bg-[#F7F8F9] p-10">
                <div className="flex flex-col gap-10 w-[95%] m-auto">
                    <div className="flex gap-2">
                        <img src={Document} className="w-[22px] h-[22px]" />
                        <p className="text-lg font-semibold">Find the right visa for you!</p>
                    </div>

                    {/* main section */}
                    <div className="flex flex-col gap-5">
                        <div className={`flex justify-between ${show ? " " : "border-[2px] border-x-0 border-t-0 border-b-[#D9D9D9] py-10"}`}>
                            <p className="text-[#4AC97D] text-5xl">Diversity Visa Cost</p>
                            {
                                show ? <img src={UP} className='w-[22px] h-[22px]' onClick={() => setShow(!show)}/> : <img src={Down} className='w-[22px] h-[22px]' onClick={() => setShow(!show)} />
                            }
                            
                        </div>

                        {/* main content */}
                        {
                            show ? <>
                                <div className="p-7 bg-white rounded-[40px] gap-10 flex flex-col">
                                    <div className="flex justify-between items-center border-[6px] border-r-0 border-y-0 border-l-[#00579B] p-2">
                                        <p className="text-lg">Mandatory Government Fees for Green Cards</p>
                                        <button className="bg-[#FD816B] text-2xl font-semibold px-[39px] py-[13px] rounded-xl text-white">
                                            Apply Now
                                        </button>

                                    </div>

                                    {/* table view */}
                                    <table className="w-full">
                                        <thead >
                                            <tr className="border-[1px] border-x-0 border-t-0 border-b-[#D9D9D9] text-start">
                                                <th className="p-4 text-lg font-semibold text-start">Fee Type</th>
                                                <th className="p-4 text-lg font-semibold text-start">Cost (to Applicat Living in the U.S.)</th>
                                                <th className="p-4 text-lg font-semibold text-start">Cost (to Applicant Living Abroad)</th>

                                            </tr>

                                        </thead>
                                        <tbody>

                                            {
                                                DATA.map(() => {
                                                    return <tr className="border-[1px] border-x-0 border-t-0 border-b-[#D9D9D9] text-start">
                                                        <td className="p-4 text-lg">Family Sponsorship Form (I-130)</td>
                                                        <td className="p-4 text-lg">$535</td>
                                                        <td className="p-4 text-lg">$535</td>
                                                    </tr>

                                                })
                                            }

                                            {/* total */}
                                            <tr className="border-[1px] border-x-0 border-t-0 border-b-[#D9D9D9] text-start">
                                                <td className="p-4 text-lg font-semibold">Total</td>
                                                <td className="p-4 text-lg font-semibold">$535</td>
                                                <td className="p-4 text-lg font-semibold">$535</td>
                                            </tr>




                                        </tbody>
                                    </table>

                                </div>


                                <div className="flex justify-end">
                                    <p className="font-[300px] text-base">Fees are subject to change, please use this calculator</p>
                                </div></> : <></>
                        }


                        {/* the other ones */}

                        <div className="flex justify-between border-[2px] border-x-0 border-t-0 border-b-[#D9D9D9] py-10">
                            <p className="text-[#4AC97D] text-5xl">Diversity Visa Timeline</p>
                            <img src={Down} className='w-[26px] h-[22px]' />
                        </div>

                        <div className="flex justify-between border-[2px] border-x-0 border-t-0 border-b-[#D9D9D9] py-10">
                            <p className="text-[#4AC97D] text-5xl">Diversity Visa FAQs</p>
                            <img src={Down} className='w-[26px] h-[22px]' />
                        </div>

                    </div>

                </div>


            </div>

            {/* third section */}
            <div className="flex flex-col p-10 bg-white">
                <div className="flex flex-col w-[95%] m-auto gap-5 border-[1px] border-x-0 border-t-0 border-b-[#D9D9D9] pb-10">
                    <div className="flex gap-3">
                        <img src={OutLineStar} className="w-[26px] h-[22px]" />
                        <p className="text-lg font-bold">Our job seekers for our selves</p>

                    </div>

                    <div className="flex justify-end">
                        <img src={TrustLogo} className="w-[176px] h-[73px]" />

                    </div>

                    <div className="flex flex-col gap-3 md:flex-row">
                        <div className="flex flex-col w-1/2 gap-3">

                            {/* profile section one */}
                            <div className="flex gap-4 border-[1px] border-x-0 border-t-0 border-b-[#D9D9D9] pb-6">
                                <div className="flex justify-center">
                                    <img src={Profile} className='h-fit' />
                                </div>

                                <div className="flex flex-col gap-4 text-[#535353] ">
                                    <p className="text-2xl font-semibold underline">Yeonseo Choi</p>
                                    <div className="flex gap-8">
                                        <p className="text-2xl font-semibold">1 Review</p>
                                        <div className="flex items-center justify-center gap-2">
                                            <img src={Location} className="w-[24px] h-[24px]" />
                                            <p className="text-2xl font-semibold">KR</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/* date */}
                            <div className="flex justify-end">
                                <p className="text-lg">Mar 20, 2023</p>
                            </div>

                            {/* stars */}
                            <div className="flex justify-start">
                                <img src={GroupStars} />
                                <img src={GroupStars} />
                                <img src={GroupStars} />
                                <img src={GroupStars} />
                                <img src={GroupStars} />

                            </div>

                            <div className="flex flex-col gap-10">
                                <p className="text-2xl font-semibold">Highly Recommend</p>
                                <p className="text-lg">Nikka was a very professional consultant, always ready to assist us each step of the way. I was
                                    <br />reminded of everthing and I had to take with to the interview and also later to pick my passport
                                    <br />and my son`s. Thanks once more, I certainly recommend the service.</p>
                                <p className="text-xl font-semibold">Date of experience: February 28, 2023</p>
                            </div>

                        </div>

                        <div className="flex justify-center w-1/2">
                            <img src={SideImage} />
                        </div>
                    </div>
                </div>

                <div className="flex flex-col w-[95%] m-auto gap-4 py-10">
                    <p className="text-[54px] text-[#4AC97D]">Let's keep in touch for news</p>

                    <div className="border-[6px] border-y-0 border-r-0 border-l-[#00579B]  rounded-xl bg-[#F5F5F5] flex px-11 py-10">
                        <p className="w-1/2 text-xl font-semibold">Subscribe to be the first do receive updates and be in advantage on your application proccess</p>
                        <div className="relative w-1/2 h-10 p-3">
                            <div className="relative h-10">
                                <input className="text-[#717579] text-lg w-full bg-white p-6 h-full flex justify-between items-center rounded-2xl border-none" placeholder="search here..."
                                />

                                <button className="bg-[#FD816B] text-white font-bold text-base px-3 py-2 absolute rounded-2xl top-1.5 right-2">Subscribe</button>

                            </div>


                        </div>

                    </div>
                </div>


            </div>

            {/* footer */}
            <div className="flex flex-col bg-[#002A54]">
                <div className="flex flex-col w-[95%] m-auto gap-3">
                    <div className="flex justify-between items-center border-[1px] border-x-0 border-t-0 border-b-[#D2D2D2] pb-3">
                        <img src={Logo} width="208" height="104" />

                        <div className="flex justify-between gap-4">
                            <a className="font-semibold text-white underline ">Terms of Service</a>
                            <a className="font-semibold text-white underline ">Terms of Service</a>
                            <a className="font-semibold text-white underline ">Terms of Service</a>
                            <a className="font-semibold text-white underline ">Terms of Service</a>
                            <a className="font-semibold text-white underline ">Terms of Service</a>

                        </div>

                    </div>

                    <p className="font-[400px]  text-justify text-white">Disclaimer: GovAssist is not affiliated with any United States government agency or department. Costs for consulting services do NOT include any government application, medical examination, filing, or biometric fees. This website does not provide legal advice and we are not a law firm. None of our customer service representatives are lawyers and they also do not provide legal advice. We are a private, internet-based travel and immigration consultancy provider dedicated to helping individuals travel to the United States. You may apply by yourself directly at travel.state.gov or at uscis.gov. GovAssist is affiliated with the UT law firm GovAssist Legal which provides legal services on immigration matters. Only licensed immigration professionals can provide advice, explanation, opinion, or recommendation about possible legal rights, remedies, defenses, options, selection of forms or strategies.</p>

                    <div className="bg-[#011527] py-5 flex justify-center items-center">
                        <p className="text-white">Copyright © 2016-2023 GovAssist, LLC All Rights Reserved</p>

                    </div>
                </div>


            </div>



        </div>
    );
}

export default Home;
